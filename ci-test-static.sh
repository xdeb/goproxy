#!/bin/bash

if [ "$HOSTNAME" = "${HOSTNAME#runner-}" ] # Don't print HOSTNAME on GitLab Runner.
then
    PS4='\n\[\033[1;93m\]${HOSTNAME}:${0}:${LINENO} + \e[m\]'
else
    PS4='\n\[\033[1;93m\]${0}:${LINENO} + \e[m\]'
fi

set -eux


################
##            ##
##  DO TESTS  ##
##            ##
################

# static linked binary
apk --no-cache add go
./dist/alpine/amd64/goproxy -version | grep "^goproxy $PKG_VERSION$"
./dist/alpine/386/goproxy -version | grep "^goproxy $PKG_VERSION$"
apk del go

# install .apk package (x86_64)
apk --allow-untrusted add ./dist/goproxy_${PKG_VERSION}-${PKG_RELEASE}-xdeb_x86_64.apk
/usr/bin/goproxy -version | grep "^goproxy $PKG_VERSION$"

# uninstall .apk package (x86_64)
test -x /usr/bin/goproxy
apk del goproxy
test ! -x /usr/bin/goproxy

# install .apk package (x86)
apk --allow-untrusted add ./dist/goproxy_${PKG_VERSION}-${PKG_RELEASE}-xdeb_x86.apk
/usr/bin/goproxy -version | grep "^goproxy $PKG_VERSION$"

# uninstall .apk package (x86)
test -x /usr/bin/goproxy
apk del goproxy
test ! -x /usr/bin/goproxy
