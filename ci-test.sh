#!/bin/bash

if [ "$HOSTNAME" = "${HOSTNAME#runner-}" ] # Don't print HOSTNAME on GitLab Runner.
then
    PS4='\n\[\033[1;93m\]${HOSTNAME}:${0}:${LINENO} + \e[m\]'
else
    PS4='\n\[\033[1;93m\]${0}:${LINENO} + \e[m\]'
fi

. /etc/os-release

set -eux


##########################
##                      ##
##  SET UP TESTING ENV  ##
##                      ##
##########################

case $ID in
debian|ubuntu)
    codename=$VERSION_CODENAME
    ;;
*)
    echo "ERROR: case not found: ID='$ID'"
    exit 1
esac


################
##            ##
##  DO TESTS  ##
##            ##
################

apt-get update
apt-get install --no-install-recommends -y golang

# dynamic linked binary
case $codename in
buster|bullseye|focal)
    ./dist/bullseye/amd64/goproxy -version | grep "^goproxy $PKG_VERSION$" ;;
bookworm|jammy|lunar|mantic)
    ./dist/bookworm/amd64/goproxy -version | grep "^goproxy $PKG_VERSION$" ;;
*)
    echo "ERROR:\ codename not found:\ $codename"
    exit 1 ;;
esac
./dist/bullseye/386/goproxy -version | grep "^goproxy $PKG_VERSION$"
./dist/bookworm/386/goproxy -version | grep "^goproxy $PKG_VERSION$"

# static linked binary
./dist/alpine/amd64/goproxy -version | grep "^goproxy $PKG_VERSION$"
./dist/alpine/386/goproxy -version | grep "^goproxy $PKG_VERSION$"

apt-get autoremove --purge --yes golang

# install .deb package (dynamic)
apt-get install --no-install-recommends -y ./dist/goproxy_${PKG_VERSION}-${PKG_RELEASE}-xdeb~${codename}_amd64.deb
/usr/bin/goproxy -version | grep "^goproxy $PKG_VERSION$"

# uninstall .deb package (dynamic)
test -x /usr/bin/goproxy
apt-get autoremove --purge --yes goproxy
test ! -x /usr/bin/goproxy

# install .deb package (static)
apt-get install --no-install-recommends -y ./dist/goproxy-static_${PKG_VERSION}-${PKG_RELEASE}-xdeb_amd64.deb
/usr/bin/goproxy -version | grep "^goproxy $PKG_VERSION$"

# uninstall .deb package (static)
test -x /usr/bin/goproxy
apt-get autoremove --purge --yes goproxy-static
test ! -x /usr/bin/goproxy
