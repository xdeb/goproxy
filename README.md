## goproxy

[goproxy](https://github.com/goproxyio/goproxy) is a Go programming language modules proxy.

This repository provides the latest Debian/Ubuntu package.

## Usage (APT installation)

1. Set up the APT key

    Add the repository's APT Key by following command:

        wget -O /etc/apt/trusted.gpg.d/xdeb-gitlab-io.gpg https://xdeb.gitlab.io/goproxy/pubkey.gpg

    or (if you prefer text formatted key file)

        wget -O /etc/apt/trusted.gpg.d/xdeb-gitlab-io.asc https://xdeb.gitlab.io/goproxy/pubkey.asc

2. Set up APT sources

    Create a new source list file at `/etc/apt/sources.list.d/xdeb-gitlab-io.list` with the
    content as following (debian/bullseye for example):

        deb https://xdeb.gitlab.io/goproxy/debian bullseye contrib

    The avaliable source list files are:

    * Debian 10 (buster)

        deb https://xdeb.gitlab.io/goproxy/debian buster contrib

    * Debian 11 (bullseye)

        deb https://xdeb.gitlab.io/goproxy/debian bullseye contrib

    * Debian 12 (bookworm)

        deb https://xdeb.gitlab.io/goproxy/debian bookworm contrib

    * Ubuntu 20.04 TLS (Focal Fossa)

        deb https://xdeb.gitlab.io/goproxy/ubuntu focal universe

    * Ubuntu 22.04 TLS (Jammy Jellyfish)

        deb https://xdeb.gitlab.io/goproxy/ubuntu jammy universe

    * Ubuntu 23.04 (Lunar Lobster)

        deb https://xdeb.gitlab.io/goproxy/ubuntu lunar universe

    * Ubuntu 23.10 (Mantic Minotaur)

        deb https://xdeb.gitlab.io/goproxy/ubuntu mantic universe

3. Install `goproxy`

        apt-get update && apt-get install goproxy

## Usage (download binary)

All binaries are stored at `https://xdeb.gitlab.io/goproxy/goproxy_<OS>_<ARCH>.gz`.

For example, you can download x64 binary for _Debian 12_ at <https://xdeb.gitlab.io/goproxy/goproxy_bookworm_amd64.gz>. You can also download i386 binary for _Ubuntu 22.04_ at <https://xdeb.gitlab.io/goproxy/goproxy_jammy_i386.gz>. Then unzip the downloaded file by `gzip -d` command. Change the file permission by `chmod` command finally.

For instance, download the binary on _Debian 12 (amd64)_, and install it, we can use a one-line command like,

    wget -O- https://xdeb.gitlab.io/goproxy/goproxy_bookworm_amd64.gz | gzip -d > /usr/local/bin/goproxy && chmod 755 /usr/local/bin/goproxy
