#!/bin/bash

if [ "$HOSTNAME" = "${HOSTNAME#runner-}" ] # Don't print HOSTNAME on GitLab Runner.
then
    PS4='\n\[\033[1;94m\]${HOSTNAME}:${0}:${LINENO} + \e[m\]'
else
    PS4='\n\[\033[1;94m\]${0}:${LINENO} + \e[m\]'
fi

. /etc/os-release

set -eux


###########################
##                       ##
##  SET UP BUILDING ENV  ##
##                       ##
###########################

case $ID in
debian)
    case $VERSION_CODENAME in
    bullseye|bookworm)
        CODENAME=$VERSION_CODENAME
        ;;
    *)
        echo "ERROR: VERSION_CODENAME not found: $VERSION_CODENAME"
        exit 1
        ;;
    esac
    ;;
alpine)
    CODENAME=alpine
    ;;
*)
    echo "ERROR: ID not found: $ID"
    exit 1
    ;;
esac
export CODENAME

src_dir=$CI_PROJECT_DIR/${PKG_NAME}-${PKG_VERSION}
dist_dir=$CI_PROJECT_DIR/dist/$CODENAME

# Prepare required commands
case $CODENAME in
bullseye|bookworm)
    apt-get update
    command -v envsubst >/dev/null || apt-get install --yes --no-install-recommends gettext
    command -v file >/dev/null || apt-get install --yes file
    command -v wget >/dev/null || apt-get install --yes wget
    ;;
alpine)
    command -v envsubst >/dev/null || apk add gettext
    command -v file >/dev/null || apk add file
    command -v wget >/dev/null || apk add wget
    ;;
*)
    echo "ERROR: case not found: CODENAME='$CODENAME'"
    exit 1
esac

# Prepare required command: nfpm
command -v nfpm >/dev/null || wget --no-verbose -O- https://xdeb.gitlab.io/nfpm/nfpm-static_amd64.tar.gz | tar -C /usr/local/bin -xz
nfpm --version

cd $src_dir


# ###############
# ##           ##
# ##  DO TEST  ##
# ##           ##
# ###############
#
# # FIXME: do a real TEST.
# go fmt $(go list ./... | grep -v /vendor/) || echo "******** TEST FAILED ********"
# go vet $(go list ./... | grep -v /vendor/) || echo "******** TEST FAILED ********"
# go test -race $(go list ./... | grep -v /vendor/) || echo "******** TEST FAILED ********"

################
##            ##
##  DO BUILD  ##
##            ##
################

ldflags="-s -w -buildid=''"
ldflags="$ldflags -X 'main.Version=${PKG_VERSION}'"

GOARCH=amd64 go build -o $dist_dir/amd64/goproxy -ldflags="$ldflags" -trimpath
GOARCH=386   go build -o $dist_dir/386/goproxy   -ldflags="$ldflags" -trimpath

$dist_dir/amd64/goproxy -version
$dist_dir/386/goproxy -version

file $dist_dir/amd64/goproxy
file $dist_dir/386/goproxy

case $CODENAME in
bullseye|bookworm)
    ldd $dist_dir/amd64/goproxy
    ! ldd $dist_dir/386/goproxy
    ;;
alpine)
    ! ldd $dist_dir/amd64/goproxy
    ! ldd $dist_dir/386/goproxy
    ;;
*)
    echo "ERROR: case not found: CODENAME='$CODENAME'"
    exit 1
esac

##################
##              ##
##  DO PACKING  ##
##              ##
##################

cd $CI_PROJECT_DIR

# import the sign key
test -n "$PRIVATE_SIGKEY"  # ensure $PRIVATE_SIGKEY is not empty
echo "$PRIVATE_SIGKEY" | base64 -d > key.gpg

mkdir -p dist/noarch
cp -v $src_dir/README.md dist/noarch/README.md
cp -v $src_dir/LICENSE   dist/noarch/LICENSE

case $CODENAME in
bullseye)
    PKG_ARCH=amd64 TARGET_CODENAME=buster envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin
    PKG_ARCH=386   TARGET_CODENAME=buster envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin

    PKG_ARCH=amd64 TARGET_CODENAME=bullseye envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin
    PKG_ARCH=386   TARGET_CODENAME=bullseye envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin

    PKG_ARCH=amd64 TARGET_CODENAME=focal envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin
    PKG_ARCH=386   TARGET_CODENAME=focal envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin
    ;;
bookworm)
    PKG_ARCH=amd64 TARGET_CODENAME=bookworm envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin
    PKG_ARCH=386   TARGET_CODENAME=bookworm envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin

    PKG_ARCH=amd64 TARGET_CODENAME=jammy envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin
    PKG_ARCH=386   TARGET_CODENAME=jammy envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin

    PKG_ARCH=amd64 TARGET_CODENAME=lunar envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin
    PKG_ARCH=386   TARGET_CODENAME=lunar envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin

    PKG_ARCH=amd64 TARGET_CODENAME=mantic envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin
    PKG_ARCH=386   TARGET_CODENAME=mantic envsubst < nfpm.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin
    ;;
alpine)
    PKG_ARCH=amd64 envsubst < nfpm-static.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin
    PKG_ARCH=386   envsubst < nfpm-static.yaml | nfpm pkg -p deb -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin

    sed -i 's/${PKG_RELEASE}-xdeb~${TARGET_CODENAME}/${PKG_RELEASE}-xdeb/' nfpm.yaml
    sed -i '/^replaces:/d;/^conflicts:/d;/^  - ${PKG_NAME}-static/d' nfpm.yaml
    PKG_ARCH=amd64 envsubst < nfpm.yaml | nfpm pkg -p apk -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin
    PKG_ARCH=386   envsubst < nfpm.yaml | nfpm pkg -p apk -t ${CI_PROJECT_DIR}/dist/ -f /dev/stdin
    ;;
*)
    echo "ERROR: case not found: CODENAME=$CODENAME"
    exit 1
esac
